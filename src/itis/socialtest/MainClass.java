package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) throws Exception {
        String way1 = "C:/Users/Админ/IdeaProjects/socialnetworktest2/src/itis/socialtest/resources/PostDatabase.csv";
        String way2 = "C:/Users/Админ/IdeaProjects/socialnetworktest2/src/itis/socialtest/resources/Authors.csv";

        new MainClass().run(way1, way2);

    }

    private void run(String postsSourcePath, String authorsSourcePath) throws  Exception{
        FileReader fileReader1 = new FileReader(postsSourcePath);
        FileReader fileReader2 = new FileReader(authorsSourcePath);
        BufferedReader bufferedReader1 = new BufferedReader(fileReader1);
        BufferedReader bufferedReader2 = new BufferedReader(fileReader2);
        ArrayList<Author> authorArrayList = new ArrayList<>();
        ArrayList<Post>  postArrayList = new ArrayList<>();
        String author = bufferedReader2.readLine();
        while (author != null) {
            String[] authors = author.split(", ");
            Author authorPath = new Author(Long.valueOf(authors[0]), authors[1], authors[2]);
            authorArrayList.add(authorPath);
            author = bufferedReader2.readLine();
        }
        String post = bufferedReader1.readLine();
        while (post != null) {
            String[] posts = post.split(", ");
            int id = Integer.parseInt (posts[0]);
            Post postPath = new Post(posts[2],posts[3], Long.valueOf(posts[1]), authorArrayList.get(id-1));
            System.out.println(postPath);
            postArrayList.add(postPath);
            post = bufferedReader1.readLine();
        }
        AnalyticsServiceImpl analyticsService = new AnalyticsServiceImpl();
        System.out.println(analyticsService.findPostsByDate(postArrayList, "17.04.2021T10:00"));
        System.out.println(analyticsService.checkPostsThatContainsSearchString(postArrayList, "Россия"));
        System.out.println(analyticsService.findAllPostsByAuthorNickname(postArrayList, "varlamov"));



    }
}



